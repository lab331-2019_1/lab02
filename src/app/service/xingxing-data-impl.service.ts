import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class XingxingDataImplService extends StudentService{

  constructor() {
    super()
   }
   getStudents(): Observable<Student[]>{
    return of (this.students);
  }
  students:Student[] = [{
    'id': 3,
    'studentId': '602115516',
    'name': 'Xingchen',
    'surname': 'Nian',
    'gpa': 3.24
  }];
  averageGpa(): number {
    let sum = 0;
    for (let student of this.students) {
      sum += student.gpa;
    }
    return sum / this.students.length;

  }
}
