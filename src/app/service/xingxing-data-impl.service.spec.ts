import { TestBed, inject } from '@angular/core/testing';

import { XingxingDataImplService } from './xingxing-data-impl.service';

describe('XingxingDataImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XingxingDataImplService]
    });
  });

  it('should be created', inject([XingxingDataImplService], (service: XingxingDataImplService) => {
    expect(service).toBeTruthy();
  }));
});
